import requests
import logging
import boto3
from botocore.exceptions import ClientError
import os
import gzip
import shutil
import json
import re
from json2html import *
import datetime

def download_file(url):
    local_filename = tempPath + "/" + url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192): 
                f.write(chunk)
    return local_filename

def upload_file(file_name, bucket, object_name=None, extra_args=None):
    if object_name is None:
        object_name = os.path.basename(file_name)
    s3_client = boto3.client('s3')
    try:
        if extra_args is None:
            extra_args = {}
        else:
            extra_args = {'ContentType':'text/html'}

        response = s3_client.upload_file(file_name, bucket, object_name, ExtraArgs=extra_args)
    except ClientError as e:
        logging.error(e)
        return False
    return True

def gunzip(source, target):
    with open(target, 'wb') as f_out, gzip.open(source, 'rb') as f_in:
        shutil.copyfileobj(f_in, f_out)

def extract(string2extract, file_name):
    file = open(file_name,"r")
    for line in file:
        if re.search(string2extract, line):
            return(line)

def create_table(json_data):
    return(json2html.convert(json = json_data))

def writeFile(data, file_name):
    text_file = open(file_name, "w")
    n = text_file.write(data)
    text_file.close()

url    = 'http://bulk.openweathermap.org/sample/weather_16.json.gz'
bucketData = os.environ.get('BUCKET_DATA')
bucketWeb = os.environ.get('BUCKET_WEB')
tempPath = "./tmp"
timestamp = datetime.datetime.now().isoformat()

if not os.path.exists(tempPath):
    os.makedirs(tempPath)

file_name = download_file(url)
gunzip(file_name,tempPath + "/file.json")
extractedData = extract('"' + os.environ.get('CITY') + '"',tempPath + "/file.json")
htmlTable = create_table(extractedData)
writeFile(htmlTable, tempPath + "/index.html")
upload_file(tempPath + "/index.html",bucketWeb,"index.html","html")
upload_file(file_name,bucketData,"package-" + timestamp + ".gz")