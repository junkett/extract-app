FROM centos:latest

ARG USER=centos
ARG V_ENV=boto3venv

USER root

RUN yum install -y epel-release

RUN yum install -y \
        python3-pip
# Upgrade pip
RUN pip3 install --upgrade pip

# create user
RUN useradd -ms /bin/bash ${USER}
USER ${USER}
WORKDIR /home/${USER}
COPY app .

RUN mkdir -p /home/${USER}/.aws
COPY aws/config /home/${USER}/.aws

RUN pip3 install virtualenv --user

# create virtual environment
RUN /home/${USER}/.local/bin/virtualenv ${V_ENV}

ENV PYTHONPATH=/home/${USER}/app

# set PATH to python3, pip3
ENV PATH=/home/${USER}/${V_ENV}/bin:$PATH

# install python packages using pip in the virtual environment
COPY requirements.txt .
RUN pip3 install -r requirements.txt

ENTRYPOINT python g.py
